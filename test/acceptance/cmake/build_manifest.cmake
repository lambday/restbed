# Copyright (c) 2013, 2014, 2015 Corvusoft

set( MANIFEST
     ${SOURCE_DIR}/test_logger.cpp
     ${SOURCE_DIR}/test_service.cpp
     ${SOURCE_DIR}/service_helper.cpp
)
